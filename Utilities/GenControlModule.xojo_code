#tag Module
Protected Module GenControlModule
	#tag Method, Flags = &h0
		Function extract_Field_Name(field As String) As String
		  
		  // Extraire le nom du champ en laissant le suffixe avec le souligné
		  
		  Dim fieldTable(-1) As String = Split(field, "_")
		  
		  Dim indexLastElement As Integer = Ubound(fieldTable)
		  
		  Return Replace(field, "_"+fieldTable(IndexLastElement), "")
		  
		End Function
	#tag EndMethod


	#tag Constant, Name = CARRIAGERETURN, Type = String, Dynamic = True, Default = \"\r", Scope = Public
	#tag EndConstant

	#tag Constant, Name = CENTRALDATABASENAMEPROD, Type = String, Dynamic = True, Default = \"techeol", Scope = Public
	#tag EndConstant

	#tag Constant, Name = CENTRALDATABASENAMETEST, Type = String, Dynamic = True, Default = \"techeol", Scope = Public
	#tag EndConstant

	#tag Constant, Name = CMEQDATABASENAMEPREPROD, Type = String, Dynamic = True, Default = \"Cie05", Scope = Public
	#tag EndConstant

	#tag Constant, Name = CMEQDATABASENAMEPROD, Type = String, Dynamic = True, Default = \"Cie05", Scope = Public
	#tag EndConstant

	#tag Constant, Name = CMEQDATABASENAMETEST, Type = String, Dynamic = True, Default = \"CMEQ Cie05 32 bits", Scope = Public
	#tag EndConstant

	#tag Constant, Name = HOSTPHOTOPROD, Type = String, Dynamic = True, Default = \"", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"ml6cartier01.cartierenergie.com"
	#tag EndConstant

	#tag Constant, Name = HOSTPHOTOTEST, Type = String, Dynamic = True, Default = \"192.168.1.86", Scope = Public
	#tag EndConstant

	#tag Constant, Name = HOSTPREPRODUCTION, Type = String, Dynamic = True, Default = \"192.168.1.11", Scope = Public
	#tag EndConstant

	#tag Constant, Name = HOSTPROD, Type = String, Dynamic = True, Default = \"192.168.1.10", Scope = Public
	#tag EndConstant

	#tag Constant, Name = HOSTTEST, Type = String, Dynamic = True, Default = \"192.168.1.86", Scope = Public
	#tag EndConstant

	#tag Constant, Name = LEAVEAPPMESSAGE, Type = String, Dynamic = True, Default = \"Vous \xC3\xAAtes en train de quitter cette application.\r", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Vous \xC3\xAAtes en train de quitter cette application."
		#Tag Instance, Platform = Any, Language = en, Definition  = \"You are about to leave this application."
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Vous \xC3\xAAtes en train de quitter cette application."
	#tag EndConstant

	#tag Constant, Name = PASSWORDCMEQ, Type = String, Dynamic = True, Default = \"", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"sql"
	#tag EndConstant

	#tag Constant, Name = PASSWORDTECHEOL, Type = String, Dynamic = True, Default = \"", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"postgres"
	#tag EndConstant

	#tag Constant, Name = PHOTOFOLDER, Type = String, Dynamic = True, Default = \"baseplan/reppale", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"baseplan/reppale"
	#tag EndConstant

	#tag Constant, Name = USERCMEQ, Type = String, Dynamic = True, Default = \"", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"dba"
	#tag EndConstant

	#tag Constant, Name = USERTECHEOL, Type = String, Dynamic = True, Default = \"", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"admin"
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Module
#tag EndModule
